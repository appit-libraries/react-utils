import {get} from 'object-path-immutable';

export const mapResult = (data, ClassName, extraData = {}) => {
	return mapResultMulti(data, (data) => new ClassName(data), extraData);
};
export const mapResultMulti = (data, instanciator, extraData = {}) => {
	if(data === undefined){
		throw new Error('Data is undefined');
	}
	if(Array.isArray(data)){
		return data.map((i) => mapResultMulti(i, instanciator, extraData));
	}
	if(data === null){
		return null;
	}

	return instanciator({
		...data,
		...extraData,
	});
};
export const isClass = (func) => {
	return typeof func === 'function' && /^class\s/.test(Function.prototype.toString.call(func));
};
export const ifNull = (target, onNull, needToBeCalled = false) => {
	if(target === null){
		if(needToBeCalled){
			return onNull();
		}
		return onNull;
	}

	return target;
};
/**
 *
 * @param value
 */
export const expectsString = (value) => {
	console.assert(typeof value === 'string', `String is expected instead of ${typeof value}`);
};
/**
 *
 * @param value
 */
export const expectsDate = (value) => {
	console.assert(value instanceof Date, `Date is expected instead of ${typeof value}`);
};
/**
 *
 * @param value
 */
export const expectsObject = (value) => {
	console.assert(value instanceof Object, `Object is expected instead of ${typeof value}`);
};
export const dateOrNull = (value) => ifNotNull(throwIfUndefined(value), Date);
export const ifEmpty = (target, onEmpty, needToBeCalled = false) => {
	if(!target){
		if(needToBeCalled){
			return onEmpty();
		}
		return onEmpty;
	}

	return target;
};
export const parseIfJson = (str, onNotJson = a => a) => {
	try{
		return JSON.parse(str);
	}catch(e){
		return callIfFunction(onNotJson, str);
	}
};
export const ifNotEmpty = (target, onNotEmpty, needToBeCalled = false) => {
	if(!!target){
		if(needToBeCalled){
			return onNotEmpty(target);
		}
		return onNotEmpty;
	}

	return target;
};
export const ifNotNull = (target, onNotNull) => {
	if(target === null){
		return null;
	}

	return onNotNull(target);
};
export const callIfFunction = (value, ...args) => {
	if(typeof value === 'function'){
		return value(...args);
	}

	return value;
};
export const throwIfUndefined = (value, message?: string) => {
	if(value === undefined || typeof value === 'undefined'){
		throw new Error(message);
	}

	return value;
};
export const getOrThrow = (data: Object, prop: string) => {
	if(data[prop] === undefined){
		throw new Error(`Impossible to retrive ${prop} from data(available properties ${Object.keys(data)
			.join(', ')})`);
	}

	return data[prop];
};
export const asset = (src = '', isSecure) => {
	if(isSecure === undefined){
		switch(process.env.REACT_APP_ASSETS_SECURE){
			case 'false':
			case false:
				isSecure = false
				break;
			case 'true':
			case true:
				isSecure = false
				break;
		}
	}

	let secure = ""
	if(isSecure === true){
		secure = 'https:';
	}
	if(isSecure === false){
		secure = 'http:';
	}

	if(src === null){
		return '';
	}

	const host = process.env.REACT_APP_ASSETS_HOST || process.env.REACT_APP_API_HOST

	return secure + '//' + host + '/' + src;
};
export const promiseTimeout = (time) => {
	return new Promise(function(resolve, reject){
		setTimeout(function(){
			resolve(time);
		}, time);
	});
};
export const objectToFormData = (obj, form, namespace) => {
	const fd = !!form ? form : new FormData();
	let formKey;
	for(const property in obj){
		if(obj.hasOwnProperty(property)){
			if(namespace){
				formKey = namespace + '[' + property + ']';
			}
			else{
				formKey = property;
			}
			// if the property is an object, but not a File,
			// use recursivity.
			if(typeof obj[property] === 'object' && !(obj[property] instanceof File)){
				objectToFormData(obj[property], fd, formKey);
			}
			else{
				// if it's a string or a File object
				fd.append(formKey, obj[property]);
			}
		}
	}

	return fd;
};
export const toFixedLocale = (number, min = 2, max = 2) => {
	if(Number.isNaN(Number(number))){
		return '';
	}
	return (Number(number).toLocaleString(undefined, {
		minimumFractionDigits: min,
		maximumFractionDigits: max,
	}));
};
export const strictObject = (obj: Object<T>): T => {
	const handler = {
		get(target, property){
			if(property in target){
				return target[property];
			}

			throw new Error(`Property '${property}' is not defined`);
		},
	};

	return new Proxy(obj, handler);
};
export const cleanObject = (obj: Object<T>): T => {
	obj = {...obj};
	for(const propName in obj){
		if(obj[propName] === null || obj[propName] === undefined || obj[propName] === ''){
			delete obj[propName];
		}
	}

	return obj;
};
export const extraCleanObject = obj => Object.entries(obj)
	.map(([k, v]) => [k, v && typeof v === 'object' ? extraCleanObject(v) : v])
	.reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {});

export const uniqueArray = (src, key, sort) => {
	var results = [];
	var values = [];
	var i = 0;

	while(i < src.length){
		var val = key ? get(src[i], key) : src[i];
		if(values.includes(val) < 0){
			results.push(src[i]);
		}
		values[i] = val;
		i++;
	}

	if(sort){
		if(!key){
			results = results.sort();
		}
		else{
			results = results.sort(function sortUniqueArray(a, b){
				return a[key] > b[key];
			});
		}
	}

	return results;
};
