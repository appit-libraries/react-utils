import {get, merge} from 'object-path-immutable';
import {useCallback, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {DispatchOptionsType, ReducerOptionArgsType, ReducerStateType} from './ReducerBasicOptionTypes';
import ReducerSettings from './ReducerSettings';

const initialState: ReducerStateType = {
	begined: false,
	progress: 0,
	counter: 0,
	data: null,
	error: undefined,
	clear: false,
	finished: false,
	touched: false,
	operation: undefined,
	last_action: undefined,
	origin: undefined,
	pagination: null,
};
const prepareAction = (actionName, args = {}) => ({
	type: actionName,
	...args,
});

export default class ReducerBasicOption {
	#name: string;
	#operation: string;
	#domain: string;
	#dispatchOptions: DispatchOptionsType;
	#callback: ReducerDataCallbackType;
	#types: ReducerActionTypes;
	#initialState: ReducerStateType = initialState;

	constructor(name: string, operation: string, {domain, dataCallback, dispatchOptions}: ReducerOptionArgsType = {}){
		if(!operation){
			throw new Error(`Reducer operation were not set`);
		}
		this.#name = name;
		this.#operation = operation;
		this.#domain = domain;
		this.#callback = dataCallback;
		this.#dispatchOptions = !dispatchOptions ? {} : dispatchOptions;
		this.#types = {
			TYPE_BEGIN: `${name.toUpperCase()}_${operation.toUpperCase()}_BEGIN`,
			TYPE_PROGRESS: `${name.toUpperCase()}_${operation.toUpperCase()}_PROGRESS`,
			TYPE_SUCCESS: `${name.toUpperCase()}_${operation.toUpperCase()}_SUCCESS`,
			TYPE_FAIL: `${name.toUpperCase()}_${operation.toUpperCase()}_FAIL`,
			TYPE_CLEAR: `${name.toUpperCase()}_${operation.toUpperCase()}_CLEAR`,
		};
	}

	get actions(){
		const types = this.#types;
		const operation = this.#operation;
		return {
			launchBegin: () => prepareAction(types.TYPE_BEGIN, {operation}),
			launchProgress: (progress) => prepareAction(types.TYPE_PROGRESS, {progress, operation}),
			launchSuccess: (data) => prepareAction(types.TYPE_SUCCESS, {data, operation}),
			launchFail: (error) => prepareAction(types.TYPE_FAIL, {error, operation}),
			launchClear: (origin) => prepareAction(types.TYPE_CLEAR, {origin, operation}),
		};
	}

	mergeInitialProp(propPath, value){
		this.#initialState = merge(this.#initialState, propPath, value);

		return this;
	}

	dispatch_clear = (realDispatch, origin) => {
		realDispatch(this.actions.launchClear(origin));
	};

	onFail(onFailCallback){
		this.#dispatchOptions.onFail = onFailCallback;

		return this;
	}

	onBegin(onBeginCallback){
		this.#dispatchOptions.onBegin = onBeginCallback;

		return this;
	}

	onEnd(onEndCallback){
		this.#dispatchOptions.onEnd = onEndCallback;

		return this;
	}

	dispatch = (realDispatch, ...args) => {
		return this.dispatch_callback(realDispatch, this.#dispatchOptions, ...args);
	};

	dispatch_callback = (realDispatch, {onBegin, onFail, onEnd, onSuccess}: DispatchOptionsType, ...args) => {
		const actions = this.actions;
		const operation = this.#operation;
		let data = this.#callback;
		if(typeof realDispatch !== 'function'){
			throw new TypeError('dispatch is not a function');
		}
		return realDispatch(
			async (dispatch, getState) => {
				if(typeof onBegin === 'function'){
					onBegin();
				}
				dispatch(actions.launchBegin());
				if(typeof data !== 'function'){
					dispatch(actions.launchSuccess(data));
				}
				else{
					try{
						data = await this.#callback({
							getState,
							dispatch,
							operation,
							setProgress: (progress) => dispatch(actions.launchProgress(progress)),
						}, ...args);
						if(!!onSuccess){
							onSuccess(data);
						}

						dispatch(actions.launchSuccess(data));
					}catch(error){
						if(error.constructor !== Error.prototype.constructor){
							Promise.reject(error);
						}
						else{
							if(typeof onFail === 'function'){
								onFail();
							}
							// console.log('reducer', error);
							dispatch(actions.launchFail(error));
						}
					}
				}
				if(typeof onEnd === 'function'){
					onEnd();
				}
			},
		);
	};

	/**
	 *
	 * @deprecated
	 * @param state
	 * @returns {ReducerStateType}
	 */
	map = (state): ReducerStateType => {
		const paths = [this.#domain, this.#name, this.#operation];

		return get(state, paths, {...this.#initialState});
	};

	useMap = (): ReducerStateType => useSelector(this.map);

	/**
	 * 
	 * @param dispatch
	 * @returns {(...args: any[]) => any}
	 */
	useCallback = (dispatch,...i_args) => {
		const callback = useCallback((...args) => {
			this.dispatch(dispatch,...i_args,...args)
		},[dispatch,...i_args]);

		return callback;
	};

	useEffect = (dispatch,...args) => {
		const effect = useCallback(() => {
			console.info('effect',this.#name)
			this.dispatch(dispatch,...args)
		},[dispatch,...args])
		useEffect(() => {
			effect()
			const clear = this.dispatch_clear;
			return () => {
				clear(dispatch)
			}
		},[dispatch,effect])

		return effect;
	}

	useEffectIf = (dispatch,cond,...args) => {
		const effect = useCallback(() => {
			this.dispatch(dispatch,...args)
		},[dispatch,...args])
		useEffect(() => {
			if(cond){
				effect()
				const clear = this.dispatch_clear;
				return () => {
					clear(dispatch)
				}
			}			
		},[dispatch,cond,effect])

		return effect;
	}

	handleBegin(state, action, handledBy = []){
		return merge(state, this.#operation, {
			touched: true,
			begined: true,
			finished: false,
			progress: 0,
			data: null,
			error: null,
			clear: false,
			last_action: this.#types.TYPE_BEGIN,
			origin: action.origin,
			pagination: null,
		});
	}

	handleProgress(state, action, handledBy = []){
		return merge(state, this.#operation, {
			touched: true,
			begined: true,
			finished: false,
			progress: action.progress,
			data: null,
			error: null,
			clear: false,
			last_action: this.#types.TYPE_PROGRESS,
			origin: action.origin,
			pagination: null,
		});
	}

	handleSuccess(state, action, handledBy = []){
		return merge(state, this.#operation, {
			touched: true,
			begined: true,
			finished: true,
			progress: 1,
			data: action.data && action.data.pagination ? action.data.result : action.data,
			counter: get(state, [this.#operation, 'counter'], 0) + 1,
			error: null,
			clear: false,
			last_action: this.#types.TYPE_SUCCESS,
			origin: action.origin,
			pagination: action.data && action.data.pagination,
		});
	}

	handleClear(state, action, handledBy = []){
		return merge(state, this.#operation, {
			touched: true,
			begined: false,
			finished: false,
			progress: 0,
			data: null,
			error: null,
			clear: true,
			last_action: this.#types.TYPE_CLEAR,
			origin: action.origin,
			pagination: null,
		});
	}

	handleFail(state, action, handleBy = []){
		if(ReducerSettings.warnings){
			console.log(action.error);
		}
		return merge(state, this.#operation, {
			touched: true,
			begined: true,
			finished: true,
			data: null,
			clear: false,
			error: action.error,
			last_action: this.#types.TYPE_FAIL,
			origin: action.origin,
			pagination: null,
		});
	}

	/**
	 * @param state
	 * @param {{type:string,progress:number,data:any,error:any,origin:string}} action
	 * @param {array} handledBy
	 * @returns {ReducerStateType}
	 */
	handle = (state, action, handledBy = []) => {
		const operation = this.#operation;
		const types = this.#types;
		if(state === undefined){
			state = {
				[operation]: {...this.#initialState},
			};
		}
		switch(action.type){
			case types.TYPE_BEGIN:
				handledBy.push(types.TYPE_BEGIN);
				return this.handleBegin(state, action, handledBy);
			case types.TYPE_PROGRESS:
				handledBy.push(types.TYPE_PROGRESS);
				return this.handleProgress(state, action, handledBy);
			case types.TYPE_SUCCESS:
				handledBy.push(types.TYPE_SUCCESS);
				return this.handleSuccess(state, action, handledBy);
			case types.TYPE_CLEAR:
				handledBy.push(types.TYPE_CLEAR);
				return this.handleClear(state, action, handledBy);
			case types.TYPE_FAIL:
				handledBy.push(types.TYPE_FAIL);
				return this.handleFail(state, action, handledBy);
			default:
				// if(action.operation !== operation){
				// 	return null;
				// }
				return state;
		}
	};
}