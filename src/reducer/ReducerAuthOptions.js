import ReducerBasicOption from './ReducerBasicOption';
import ReducerSettings from './ReducerSettings';

export default class ReducerAuthOptions {
	#check: ReducerBasicOption;
	#check_aux: ReducerBasicOption;
	#login: ReducerBasicOption;
	#logout: ReducerBasicOption;
	REDUCER_NAME: string = 'auth';

	constructor(name: string){
		this.REDUCER_NAME = name;
		this.#check = new ReducerBasicOption(this.REDUCER_NAME, 'check', {
			domain: ReducerSettings.AUTH_DOMAIN,
			dataCallback: ({getState}) => {
				return this.promiseUser({
					getFromStorage: () => {
						return this._loadToken();
					},
					getFromState: () => {
						return this.#login.map(getState()).data;
					},
				}).then(data => {
					// console.log("check",data)

					return data;
				})
			},
		});
		this.#check_aux = new ReducerBasicOption(this.REDUCER_NAME, 'check_aux', {
			domain: ReducerSettings.AUTH_DOMAIN,
			dataCallback: ({getState}) => {
				return this.promiseUser({
					getFromStorage: () => {
						return this._loadToken();
					},
					getFromState: () => {
						return this.#login.map(getState()).data;
					},
				}).then(data => {
					// console.log("check",data)

					return data;
				})
			},
		});
		this.#login = new ReducerBasicOption(this.REDUCER_NAME, 'login', {
			domain: ReducerSettings.AUTH_DOMAIN,
			dataCallback: ({getState, dispatch}, ...args) => {
				return this.promiseToken(...args).then(token => {
					return new Promise((resolve, reject) => {
						this._saveToken(token);
						this.#check.dispatch(dispatch);
						resolve(token);
					}).then(data => {
						// console.log("login",data)

						return data;
					});
				});
			},
		});
		this.#logout = new ReducerBasicOption(this.REDUCER_NAME, 'logout', {
			domain: ReducerSettings.AUTH_DOMAIN,
			dataCallback: ({dispatch},onLogout) => {
				this._eraseToken();
				dispatch(this.#login.actions.launchClear('logout'));
				dispatch(this.#check.actions.launchClear('logout'));
				if(typeof onLogout === 'function'){
					onLogout();
				}

				return true;
			},
		});
	}

	/**
	 *
	 * @returns {{logout: ReducerBasicOption, check: ReducerBasicOption, check_aux: ReducerBasicOption, login: ReducerBasicOption}}
	 */
	get options(){
		return {
			login: this.#login,
			logout: this.#logout,
			check: this.#check,
			check_aux: this.#check_aux,
		};
	}

	get reducer(){
		const options = this.options;
		return (state, action) => {
			const handleBy = [];
			for(const methodName of Object.keys(options)){
				const option: ReducerBasicOptions = options[methodName];
				state = option.handle(state, action, handleBy);
			}

			return state;
		};
	}

	promiseUser({getFromStorage, getFromState}): Promise{
		throw new Error('getUserFromToken must be implemented');
	}

	promiseToken(...args): Promise{
		throw new Error('genereateToken must be implemented');
	}

	_eraseToken(){
		localStorage.removeItem('TOKEN');
	}

	_saveToken(token){
		localStorage.setItem('TOKEN', token);
	}

	_loadToken(){
		return localStorage.getItem('TOKEN');
	}
}