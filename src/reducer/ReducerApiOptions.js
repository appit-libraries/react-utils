import Api from '../api/Api';
import {throwIfUndefined} from '../utils/utils';
import ReducerBasicOption from './ReducerBasicOption';
import {
	DispatchOptionsType,
	ReducerDataCallbackType,
	ReducerOptionArgsType,
} from './ReducerBasicOptionTypes';
import ReducerSettings from './ReducerSettings';

export default class ReducerApiOptions {
	#apiClass: Class<Api>;
	#useGlobalToken: boolean;
	#domain: string = ReducerSettings.ENTITY_DOMAIN;
	#options = {};
	REDUCER_NAME: string;

	retrieveTokenFromStorage(){
		throw new Error("global token not implemented")
	}

	retrieveTokenFromState(getState:() => Object){
		throw new Error('retrieve token from state not implemented');
	}

	constructor(name:string, ApiClass: Class<Api>, {useGlobalToken= false, domain= ReducerSettings.ENTITY_DOMAIN}:ReducerOptionArgsType&{useGlobalToke:boolean}={}){
		this.REDUCER_NAME = name;
		this.#apiClass = throwIfUndefined(ApiClass);
		this.#useGlobalToken = !!useGlobalToken;
		this.#domain = domain;

		const options = {};

		for(const methodName of Object.getOwnPropertyNames(ApiClass.prototype)){
			if(methodName === 'constructor'){
				continue;
			}
			options[methodName] = new ReducerBasicOption(name, methodName, {
				domain: domain,
				dataCallback: ({getState, setProgress}, ...args) => {
					let token;
					if(methodName.match(/^_anon/)){
						token = 'anon'
					}
					else if(!!useGlobalToken){
						token = this.retrieveTokenFromState(getState);
					}
					else{
						token = this.retrieveTokenFromStorage();
					}
					const api: Api = new ApiClass(token, {setProgress});
					if(!(api instanceof Api)){
						throw new Error("Invalid instance of Api")
					}

					const response = api[methodName](...args);
					return response.then(response => response.data === undefined ? response : response.data);
				},
			});
		}

		this.#options = options;
	}

	options(): $ObjMap<string, ReducerBasicOption>{
		return this.#options;
	}

	addOption(operation:string, dataCallback: ReducerDataCallbackType, dispatcherOptions: DispatchOptionsType={}){
		const name = this.REDUCER_NAME;
		const domain = this.#domain;
		this.#options[operation] = new ReducerBasicOption(name, operation, {
			domain,
			dataCallback,
			dispatcherOptions,
		});

		return this;
	}

	get reducer(){
		const options = this.#options;
		return (state, action) => {
			const handleBy = [];
			for(const methodName of Object.keys(options)){
				const option: ReducerBasicOptions = options[methodName];
				state = option.handle(state, action, handleBy);
			}

			return state;
		}
	}
}