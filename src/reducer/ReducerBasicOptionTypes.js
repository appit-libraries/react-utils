
export type ReducerActionTypes = {
	TYPE_BEGIN: string;
	TYPE_PROGRESS: string;
	TYPE_SUCCESS: string;
	TYPE_FAIL: string;
	TYPE_CLEAR: string;
}
export type ReducerStateType = {
	begined: boolean;
	progress: number;
	data: any;
	error: any;
	clear: boolean,
	finished: boolean;
	touched: boolean;
	last_action: ?string;
	pagination: ?Object;
}
export type DispatchOptionsType = {
	onFail: ({ dispatch: Function, getState: Function }) => void;
	onBegin: ({ dispatch: Function, getState: Function }) => void;
	onEnd: ({ dispatch: Function, getState: Function }) => void;
}

export type ProgressCallbackType = {
	(progress): void
}

export type ReducerDataCallbackType = {
	({
		 getState: () => Object,
		 dispatch: () => any,
		 setProgress: ProgressCallbackType,
		 operation: string,
		 action: string
	 }, ...args: any[]): any
}

export type ReducerOptionArgsType = {
	domain: string;
	dataCallback: ReducerDataCallbackType;
	dispatchOptions: DispatchOptionsType;
}