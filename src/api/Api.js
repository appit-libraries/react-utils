import axios from 'axios';
import JsonEntity from '../entity/JsonEntity';
import entityResolver from '../entity/entityResolver';
import {mapResultMulti, objectToFormData, throwIfUndefined} from '../utils/utils';

export default class Api {
	_handleData = false;
	#json: AxiosInstance;
	#image: AxiosInstance;
	#multipart: AxiosInstance;
	#jsonData: AxiosInstance;

	constructor(token, {setProgress} = {}){
		const onProgress = progressEvent => {
			if(typeof setProgress === 'function'){
				setProgress(Math.floor((progressEvent.loaded * 100) / progressEvent.total));
			}
		};

		axios.defaults.headers.Accept = 'application/json';
		axios.defaults.headers['Content-Type'] = 'application/json';
		axios.defaults.onDownloadProgress = onProgress;
		axios.defaults.onUploadProgress = onProgress;
		// axios.defaults.responseType = 'json';
		let apiUrl = `${this.base_url}`;
		if(!!this.namespace){
			apiUrl += `/${this.namespace}`;
		}

		const transformReponseToObject = (response, header, callback = data => data) => {
			let data = response;
			if(typeof data === 'string'){
				data = JSON.parse(data);
				response = data;
			}
			if(data === null){
				return null;
			}
			if(data.error){
				return data;
			}
			if(this._handleData){
				data = this.handleData(response, header);
			}
			else{
				if(data['@type'] === 'hydra:Collection'){
					data = data['hydra:member'];
				}
				else{
					data = response.data === undefined ? response : response.data;
				}
			}

			if(data === null || data === undefined){
				return null;
			}

			return callback(data,response.pagination);
		};

		const transformResponse = (response, header) => transformReponseToObject(response, header, (data,pagination) => {
			if(this.entity_class === Object){
				data = {...data};
			}
			if(this.entity_class === undefined && !(data instanceof JsonEntity)){
				data = mapResultMulti(data, this.multi);
			}
			if(data.error){
				return data;
			}

			data = entityResolver(data, this.entity_class);

			if(!!pagination){
				return {result:data,pagination}
			}

			return data;
		});

		if(!!token || token === 'anon'){
			let headers = {};
			if(token !== 'anon'){
				headers.Authorization = 'Bearer ' + token;
			}
			this.#json = axios.create({
				'transformResponse': Api.transformResponses(transformResponse),
				'baseURL': apiUrl,
				'headers': headers,
			});
			this.#image = axios.create({
				'transformResponse': Api.transformResponses((response) => 'data:image;base64,' + Buffer.from(response, 'binary').toString('base64')),
				'baseURL': apiUrl,
				"responseType": "arraybuffer",
				'headers': {
					...headers,
				},
			});
			this.#jsonData = axios.create({
				'transformResponse': Api.transformResponses(transformReponseToObject),
				'baseURL': apiUrl,
				'headers': {
					...headers,
				},
			});
			this.#multipart = axios.create({
				'transformResponse': Api.transformResponses(transformResponse),
				'baseURL': apiUrl,
				'headers': {
					...headers,
					'Content-Type': 'multipart/form-data',
				},
				'transformRequest': (data) => objectToFormData(data),
			});
		}
	}

	get API_HOST(){
		return undefined;
	}

	get API_NAMESPACE(){
		return undefined;
	}

	get base_url(){
		let base = '//' + throwIfUndefined(this.API_HOST);
		if(!!this.API_NAMESPACE){
			base += '/' + this.API_NAMESPACE;
		}

		return base;
	};

	get base(): AxiosInstance{
		return axios.create({
			'baseURL': this.base_url,
			transformResponse: [].concat(
				axios.defaults.transformResponse,
				(response, headers) => {
					let data = response;
					if(data === null){
						return null;
					}
					if(typeof data === 'string'){
						data = JSON.parse(data);
						response = data;
					}

					if(response.error){
						return response;
					}

					if(data['@type'] === 'hydra:Collection'){
						data = data['hydra:member'];
					}
					else{
						data = response.data;
					}

					return data;
				},
			),
		});
	};

	get multi(){
		return (data) => data;
	}

	get entity_class(){
		return Object;
	}

	get namespace(){
		return null;
	}

	get json(){
		if(!this.#json){
			throw new Error('json is not inatialized');
		}
		return this.#json;
	}

	get image(){
		if(!this.#image){
			throw new Error('image is not inatialized');
		}
		return this.#image;
	}

	get jsonData(){
		if(!this.#jsonData){
			throw new Error('buffer is not inatialized');
		}
		return this.#jsonData;
	}

	get multipart(){
		if(!this.#multipart){
			throw new Error('multipart is not inatialized');
		}
		return this.#multipart;
	}

	static transformResponses(transformResponse: (response, header) => any){
		return [].concat(
			axios.defaults.transformResponse,
			transformResponse,
		);
	}

	handleData(response, headers){
		return response;
	}
}