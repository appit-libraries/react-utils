import JsonEntity, {JsonEntityKeys} from './JsonEntity';
import {ifEmpty, throwIfUndefined} from '../utils/utils';

const datas:$ObjMap<string,Object> = {}
const instances:$ObjMap<string,Array<JsonEntity>> = {};

const pushInstance = (id,instance) => {
	if(!instances[id]){
		instances[id] = [];
	}

	instances[id].push(instance)
}

const getData = (myData,extraData={}) => {
	if(myData instanceof JsonEntity){
		myData = myData.getData();
	}
	// const change = new Date(myData.stamp_change);
	// if(!!datas[myData.id]){
	// 	const previousData = datas[myData.id];
	// 	if(change.getTime() > new Date(previousData.stamp_change).getTime()){
	// 		myData = {
	// 			...myData,
	// 			...previousData,
	// 		}
	// 	}
	// 	else if(change.getTime() < new Date(previousData.stamp_change).getTime()){
	// 		myData = {
	// 			...previousData,
	// 			...myData,
	// 		};
	// 	}
	// }
	myData = {
		...ifEmpty(myData,{}),
		...extraData
	}

	datas[myData.id] = myData;

	return myData;
}

const entityResolver = (data, ClassSelector,extraData={}) => {
	throwIfUndefined(data,"Data must be defined")
	if(data === null){
		return null;
	}
	if(Array.isArray(data)){
		return data.map(datum => entityResolver(datum,ClassSelector,extraData));
	}
	if(typeof data === 'string'){
		data = {
			id: data
		}
	}
	if(!data.id && !!data._id){
		data.id = data._id
	}
	if(!data.id){
		// console.log(data)
		data.id = JsonEntityKeys.idGenerator(data)
	}

	if(data._circular){
		return instances[data.id];
	}

	const newData = getData(data,extraData)

	const similarInstances = ifEmpty(instances[newData.id],[]);
	for(const oldInstance:JsonEntity of similarInstances){
		if(oldInstance instanceof JsonEntity){
			// oldInstance.setData(newData)
		}
	}
	if(data instanceof JsonEntity){
		data.setData(newData);
		pushInstance(data.id,data)

		return data;
	}

	let Class;
	if(typeof ClassSelector === 'function'){
		Class = ClassSelector
	}
	else if(typeof ClassSelector === 'object'){
		Class = ClassSelector.getClass(newData)
	}

	throwIfUndefined(Class,"Class must be defined")

	const instance:JsonEntity = new Class(newData);
	pushInstance(instance.id,instance)

	return instance;
}

export default entityResolver;
