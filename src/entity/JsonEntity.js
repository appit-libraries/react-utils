import objectPath from 'object-path';
import {extraCleanObject, throwIfUndefined} from '../utils/utils';
import entityResolver from './entityResolver';

export const JsonEntityKeys = {
	converter: (key) => [key],
	idGenerator: (data) => btoa(JSON.stringify(data))
};

export default class JsonEntity{
	#data;
	constructor(data){
		this.setData(data)
	}

	getDataValue(key,ClassSelector,extraData={}){
		const keys = JsonEntityKeys.converter(key)

		let value = undefined;
		for(const k of keys){
			value = objectPath.get(this.#data,k,undefined)
			if(value !== undefined){
				break;
			}
		}

		const data = throwIfUndefined(value,`Not found key '${key}'. Keys: ${Object.keys(this.#data).join(', ')}`);

		if(ClassSelector !== undefined && data !== null){
			if(ClassSelector === Date || ClassSelector === Object || ClassSelector === Number){
				return new ClassSelector(data);
			}
			return entityResolver(data,ClassSelector,extraData)
		}

		return data;
	}

	getData(clean){
		if(clean){
			return extraCleanObject(this.#data)
		}
		return this.#data;
	}

	clone(){
		return new this(this.#data);
	}

	setData(data){
		this.#data = data;
	}

	setDataValue(key,value){
		this.#data[key] = value;
	}

	get id(){
		return this.#data.id;
	}

	get stamp_change():Date{
		 return this.getDataValue('stamp_change',Date);
	};
	get stamp_create():Date{
		 return this.getDataValue('stamp_create',Date);
	};

	is_circular(){
		return this.#data._circular;
	}
}
